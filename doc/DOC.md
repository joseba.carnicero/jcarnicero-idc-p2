# P1 - Doc

En este documento se recogen la definición y evidencias de la Práctica 2º denominda _ de la asignatura Integración y Despliegue Continuo del master Análisis de datos, Ciberseguridad y Computación en la nube.

## Pipeline 

El pipeline creado tiene 6 fases de la Integración Continua y el Despliegue Continuo:
- *build*. Se construye la aplicación flask.
- *test*. Se testea la aplicación en un entorno de pruebas.
- *delivery_canary*. Se entrega la aplicación en el repositorio con el tag de canary.
- *canary*. Se despliega el canary el el cluster.
- *delivery*. Cuando se ha combrobado el canary desplegado, se le cambia el tag a latest (imagen de producción). Además se guarda en el registro de containers la misma imagen con su versión para un posible futuro rollback. Esta fase se activa automaticamente.
- *deploy*. Se despliega la nueva imagen de producción en Gcloud y se elimina el canario. Esta fase se activa automaticamente.

El despliegue del canary se hace en la misma rama de desarrollo (branch master), y cuendo se testee lo suficiente se hace el deploy a producción manualmente actualizando la imagen de producción a la última del branch.

## Estrategia de branching

El desarrollo completo de esta práctica se realiza en la rama Master.

## Evidencias

Para el Milestone 1 se ha desarrollado el despliegue de la aplicación en un cluster de Minikube en local, todo de manera manual.

Se ha creado un configmap para guardar los camndos del script que crea las bases de datos.
<p align="center">
  <img src="images/0-1.PNG">
</p>

<p align="center">
  <img src="images/0-2.PNG">
</p>

Se ha creado un pod con el contenedor de Postgres.

<p align="center">
  <img src="images/1.PNG">
</p>

<p align="center">
  <img src="images/2.PNG">
</p>

<p align="center">
  <img src="images/3-1.PNG">
</p>

<p align="center">
  <img src="images/3-2.PNG">
</p>

Y un servicio de tipo ClusterIp para que el pod tenga un endpoint dentro del cluster que se va a crear.

<p align="center">
  <img src="images/4-1.PNG">
</p>

<p align="center">
  <img src="images/4-2.PNG">
</p>

Con la ip que se le ha asignado se puede acceder a Postgres desde una máquina que esté en el clsuter.

<p align="center">
  <img src="images/4-3.PNG">
</p>

Se crea un secreto para poder acceder al registro de contenedores de Gitlab.

<p align="center">
  <img src="images/5-1.PNG">
</p>

<p align="center">
  <img src="images/5-2.PNG">
</p>

Se despliegan 2 pods cada uno con un contenedor que lleva la aplicación.

<p align="center">
  <img src="images/5-3.PNG">
</p>

<p align="center">
  <img src="images/5-4.PNG">
</p>

Y se crea también un servicio tipo LoadBalancer que ofrecerá un endpoint para conectarse con los pods.

<p align="center">
  <img src="images/5-5.PNG">
</p>

<p align="center">
  <img src="images/5-6.PNG">
</p>

Comprobación de la aplicación desplegada.

<p align="center">
  <img src="images/5-7.PNG">
</p>

Para el Milestone 2 se han dado los mimos pasos que en el primero, pero esta vez en el servicio de Kubernetes de Google Cloud. Además se ha añadido el despliegue de un canary manualmente.

Se ha creado un service account y un rol de owner en el proyecto de Gcloud para poder crear el clsuter.

<p align="center">
  <img src="images/6-1.PNG">
</p>

<p align="center">
  <img src="images/6-2.PNG">
</p>

Se han importado las credenciales del iam para usar el CLI de Gcloud.

<p align="center">
  <img src="images/6-3.PNG">
</p>

Se ha creado el cluster en Gcloud con un nodo.

<p align="center">
  <img src="images/7-1.PNG">
</p>

<p align="center">
  <img src="images/7-2.PNG">
</p>

Se ha creado el configmap de postgres.

<p align="center">
  <img src="images/8-1.PNG">
</p>

El secret para el container registry.

<p align="center">
  <img src="images/8-2.PNG">
</p>

Los pods con los contenedores.

<p align="center">
  <img src="images/8-3.PNG">
</p>

Y los servicios.

<p align="center">
  <img src="images/8-4.PNG">
</p>

Con la ip asignada por el servicio hay acceso a la aplicación.

<p align="center">
  <img src="images/8-5.PNG">
</p>

Se ha pasado el pipeline con el delivery del contenedor del canary.

<p align="center">
  <img src="images/9-1.PNG">
</p>

Y se ha desplegado el canary manualmente.

<p align="center">
  <img src="images/9-2.PNG">
</p>

El enpoint dirige a la aplicación en producción o al canary según la distribución del load balancer.

<p align="center">
  <img src="images/9-3.PNG">
</p>

Por último se ha hecho el cambio del canary a produción activando el stage en el pipeline y se han borrado los pods del canary.

<p align="center">
  <img src="images/9-4.PNG">
</p>

<p align="center">
  <img src="images/9-6.PNG">
</p>

Las imágenes de los contenedores creados durante el pipeline.

<p align="center">
  <img src="images/9-5.PNG">
</p>

Para el Milestone 3 se han integrado los últimos pasos manuales en el pipeline automatizado.

<p align="center">
  <img src="images/10-1.PNG">
</p>

Todos los pods se despliegan automáticamente.

<p align="center">
  <img src="images/10-2.PNG">
</p>

Y se generan y tagean las nuevas imágenes generadas.

<p align="center">
  <img src="images/10-3.PNG">
</p>

Los últimos 2 stages se activan manualmente cuando se comprueba el canary.

<p align="center">
  <img src="images/10-4.PNG">
</p>

Y se pasa el canary a producción de forma automática.

<p align="center">
  <img src="images/10-5.PNG">
</p>

<p align="center">
  <img src="images/10-6.PNG">
</p>

La versión del canary queda tageada para un posible rollback manual futuro.

<p align="center">
  <img src="images/10-7.PNG">
</p>

## Gestión agile

Para la gestión y desarrollo del software se ha elegido SCRUM. El proyecto se ha dividido en 3 sprints (milestones).

Las tareas se colocan en una tabla con los Tags de to-do, doing o closed según su estado en el desarrollo y cuando todas las tareas asignadas a un sprint se cierren, este también se terminará. 

<p align="center">
  <img src="images/dev_board.PNG">
</p>

Al final de cada sprint se dedicará un tiempo a hacer el retrospective meeting, y se anotarán las conclusiones razondas.

## Restrospective meeting

**Milestone 1**

: ) 
- No ha habido dificultades al desplegar el cluster y los demás recursos gracias a los Kwiklabs sobre Kubernetes hechos de antemano.

: (
- La variables de entorno para el deploy de la aplicación deberían pasarse en un configmap.
 
**Milestone 2**

: ) 
- El desarrollo de este Sprint ha sido rápido porque solamente ha habido que replicar los pasos dados en el primer Sprint.
 
 **Milestone 3**

: ) 
- Las píldoras sobre las estrategias de pipeline han sido de mucha ayuda a la hora de definir cómo configurar el branching y el pipeline.