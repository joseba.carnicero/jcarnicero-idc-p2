# P2 – Container-based Continuous application orchestration and Deployment

[![pipeline status](https://gitlab.com/joseba.carnicero/jcarnicero-idc-p2/badges/master/pipeline.svg)](https://gitlab.com/joseba.carnicero/jcarnicero-idc-p2/commits/master)

Este repositorio contiene la Práctica 2º denimonada *Container-based Continuous applicaton orchestration and Deployment* de la asignatura **Integración y Despliegue Continuo del master Análisis de datos, Ciberseguridad y Computación en la nube**.

La práctica tiene por objetivo ser capaz de diseñar e implementar un pipeline CD con orquestación de contenedores en cloud.

Para la entrega del producto final se hayan definidos los siguientes requerimientos:
- Desplegar en local con Minikube la aplicación.
- Desplegar en cluster de GCloud la aplicación desde consola local/cloud.
- Extender el pipeline CI para automatizar el despliegue en GCloud.
- Emplear una metodología ágil para la gestión de tareas de la práctica.

## Documentación

Los detalles de las especificaciones, diseño y evidencias del desarrollo de la práctica quedan recogidos en la documentación complementaria [DOC.md](doc/DOC.md).

## Autores

Joseba Carnicero Padilla – [joseba.carnicero@alumni.mondragon.edu](mailto:joseba.carnicero@alumni.mondragon.edu)

##  Licencia

Este proyecto está bajo Licencia [MIT License](https://choosealicense.com/licenses/mit/). Ver documento [LICENSE.md](LICENSE.md) adjunto en el proyecto para más detalles.
